/**
 * Created by Administrator on 2015/7/5 0005.
 */
define('login',
    ['avalon', 'text!../../package/login/login.html', 'css!../../package/login/login.css','mmAnimate'],
    function (avalon, html, css) {
        var vm = avalon.define({
            $id: "login",
            notLogin:'',
            target:'#!/Order/0',
            state:"default",//default默认状态 waiting 等待返回 sucess成功 err出错
            animateTime:550,
            ready: function () {
                //登陆界面进入
                avalon(window.document.getElementById("loginPanel")).fadeIn(vm.animateTime)
                setTimeout(function(){
                    vm.notLogin=true
                },vm.animateTime)
                avalon(window.document.getElementById("desk")).fadeOut(vm.animateTime)

                //数据重置
                vm.state="default"
                vm.login_account= "";
                vm.login_pwd = "";
                vm.login_acctip = ''
                vm.loginAccFlag = 0
                vm.login_pwd = ''
                vm.login_pwdtip = ''
                vm.loginPwdFlag = 0
                //清除不该出现的快捷键
                try {
                    quickStart.removeBillKey()
                }
                catch (err) {
                    console.log(err.message)
                }

                //layout.url = html



                //隐藏工作区

                avalon.scan()

                //获取账户输入框的焦点
                setTimeout(function () {
                    window.document.getElementById("account").focus()
                },1000)


                //绑定密码输入框的快捷键
                vm.bindEnter()

            },
            bindEnter: function () {
                //绑定密码输入框的快捷键
                require(['../../plugins/shortcut/shortcut.js'], function () {
                    var target = window.document.getElementById("pwd")
                    shortcut.add("enter", function () {
                        vm.login()
                        shortcut.remove("enter")
                    }, {
                        type: "keyup",
                        target: target
                    })

                })
            },


            //验证登陆用户名
            login_account: '',
            login_acctip: '',
            loginAccFlag: 0,
            verifyLoginName: function () {
                if (vm.login_account == '') {
                    vm.login_acctip = '用户名不能为空';
                    vm.loginAccFlag = 1;
                }
                else {
                    vm.login_acctip = '';
                    vm.loginAccFlag = 0;
                    return 1;
                }
            },
            clearPwd:function(){
                vm.login_pwd = '';
            },
            //验证登陆密码
            login_pwd: '',
            login_pwdtip: '',
            loginPwdFlag: 0,
            verifyLoginPwd: function () {
                if (vm.login_pwd == '') {
                    vm.login_pwdtip = '密码不能为空';
                    vm.loginPwdFlag = 1;
                }
                else {
                    vm.login_pwdtip = '';
                    vm.loginPwdFlag = 0;
                    return 1;
                }
            },


            //    登录

            login: function () {
                if (vm.verifyLoginName() == 1 && vm.verifyLoginPwd() == 1) {
                    vm.loginFlag = 0;

                    vm.state="waiting"
                    ws.call({
                        i: "User/login",
                        data: {UN: vm.login_account, PWD: vm.login_pwd},
                        success: function (data) {
                            if (!data.err) {
                                vm.state="success"
                                tip.on("登录成功", 1, 3000)
                                door.logined = true;
                                cache.go({
                                    "Token": data.Token,
                                    "un": data.UN,
                                    "uid": data.UID
                                })
//                                modal.mustOut()
                                quickStart.inSide = true;
                                quickStart.user.UserName = data.UN
                                quickStart.user.UID = data.UID
                                door.locked = false;



                                quickStart.getStores()



                                //记录这次使用的地址
                                cache.go({
                                    CompanyDomian: window.location.href
                                })

                                //重新绑定该有的快捷键
                                try {
                                    quickStart.bindBillKey()
                                }
                                catch (err) {
                                    console.log(err.message)
                                }

                                //跳转首页

                                    //window.location.href = "#!/Order/0"
                                    //window.document.getElementById("topBar").style.display = "block"


                                vm.getStart()

                            }
                            ////401   用户名或密码错误
                            //else if (data.err == 401) {
                            //    vm.state="err"
                            //    tip.on('用户名或密码错误', 0, 5000);
                            //    vm.bindEnter()
                            //    setTimeout(function(){
                            //        window.document.getElementById("pwd").focus()
                            //    },300)
                            //}
                            else {
                                vm.state="err"
                                tip.on('登录失败：'+data.err, 0, 5000);
                                vm.bindEnter()
                                setTimeout(function(){
                                    window.document.getElementById("pwd").focus()
                                },300)
                            }
                        }
                    });
                }
            },

            //进入工作区
            getStart:function(){
                vm.state="success"
                //抓取上次的页面地址
                if(cache.go("lastUrl")!=undefined&&cache.go("lastUrl")!="/login"){
                    window.location.href=="#!"+cache.go("lastUrl")
                }else{
                    window.location.href=vm.target
                }


                //进入工作区域动画
                setTimeout(function(){
                    avalon(window.document.getElementById("loginPanel")).fadeOut(vm.animateTime)
                    setTimeout(function(){
                        vm.notLogin = false
                    },vm.animateTime)
                    avalon(window.document.getElementById("desk")).fadeIn(vm.animateTime)
                },300)
                //window.document.getElementById("desk").style.display = "block"

            },

            //重置
            reset: function () {
                vm.notLogin=true
                vm.state="default"
                vm.login_account= "";
                vm.login_pwd = "";
                vm.login_acctip = ''
                vm.loginAccFlag = 0
                vm.login_pwd = ''
                vm.login_pwdtip = ''
                vm.loginPwdFlag = 0
                /*取消登录，返回主页*/
                window.location.href = "http://chest.tansuyun.cn/";
            }
        })
        return login = vm
    })
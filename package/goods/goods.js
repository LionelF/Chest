/**
 * Created by Administrator on 2015/7/27 0027.
 */
define('goods', [
    'avalon',
    'text!../../package/goods/goods.html',
    'text!../../package/goods/info.html',
    'css!../../package/goods/goods.css'
], function (avalon, html, info, css) {
    var vm = avalon.define({
        $id: "goods",
        ready: function (id) {
            /*以下这句话会改变HTML*/
            layout.url = html;
            if (id) {
                vm.GoodsId = id;
                vm.computeIndex();
            }
            else {
                vm.GoodsId = -1
            }
//            avalon.scan()

            if (vm.firstTime) {
                vm.list = [];
                vm.P = 0;
                vm.T = 0;
                setTimeout(function () {
                    vm.getGoods()
                }, 1)

                vm.firstTime = false
                vm.className = "选择类型"
            }/* else {
                vm.list = [];
                vm.P = 0;
                vm.T = 0
            }*/

            vm.getClass()


        },


        firstTime: true,
        /*显示控制*/
        showBtn: -2,
        showThisBtn: function (i) {
            vm.showBtn = i
        },
        hideThisBtn: function (i) {
            vm.showBtn = -2
        },

        //添加新商品
        New: {
            Name: '',//必填
            PinYin: '',//可选 商品拼音
            Number: '',//商品号，可选
            Standard: '',//规格 可选
            Unit: '',//单位 可选
            BarCode: '',//条码 可选
            Model: '',//型号
            Price1: '',//标价
            Price0:'',//成本
            Max:"",//库存最大值
            Min:"",//库存最小值
            Virtual:0,//是否虚拟商品
            Memo:'',//商品备注
            Amount:''//初始库存

        },
        editType: function (i) {
            vm.New.Virtual=i
        },
        add: function () {
            //todo 这里添加表单的验证
            var data={};
            for(var x in vm.New){
                if(x.charAt(0)!="$"){
                    data[x]=vm.New[x]
                }
            }

            //检查价格是否大于0
            function checkPrice (){
                if((vm.New.Price1>=0||vm.New.Price1=="")&&(vm.New.Price0>=0||vm.New.Price0=="")){
                    return true;
                }
                else{
                    tip.on('标价与成本不能小于0')
                    return false
                }
            }

            if (vm.New.Name != ""&&vm.checkStoreTotal(vm.New.Min,vm.New.Max)&&checkPrice()){
                ws.call({
                    i: "Goods/add",
                    data: data,
                    success: function (res) {
                        if (res.GoodsID > 0) {
                            vm.toInfo(res.GoodsID)
                            //添加价格
                            if (vm.New.Price1 != '') {
                                for(var i=0;i<2;i++){
                                    vm.editPrice({
                                        GoodsID: res.GoodsID,
                                        Type: i,
                                        Price: vm.New['Price'+i]
                                    })
                                }

                            }
                            //添加库存
                            if(vm.New.Amount>=0){
                                var store=quickStart.nowStore
                                if(store.StoreID!=undefined&&store.StoreID!=""){
                                    ws.call({
                                        i: "Order/store",
                                        data: {
                                            //OrderCode:-4,
                                            StoreID: store.StoreID,
                                            Goods: [{
                                                GoodsID: res.GoodsID,
                                                Amount: vm.New.Amount,
                                                Memo:""
                                            }],
                                            Memo: "",//订单备注
                                        },
                                        success: function (res) {
                                            if(!res.err){
                                                tip.on('商品初始库存设置成功',1,4000)
                                            }else{
                                                tip.on('商品初始库存设置失败，请通过盘存单重新设置')
                                            }
                                        }
                                    })
                                }else{
                                    tip.on('当前库房获取失败，请重新选择当前库房或门店。')
                                }

                            }else{
                                tip.on('没有设置正确的初始库存，可以通过盘存单初始化商品库存',1)
                                console.log("没有设置初始库存")
                            }

                            vm.addOver()
                            //构建新商品插入列表 原N：30
                            ws.call({
                                i: "Goods/get",
                                data: {
                                    GoodsIDs: [res.GoodsID],
                                    P: 1,
                                    N: vm.N
                                },
                                success: function (res) {
                                    if (res.L.length) {
                                        vm.list.unshift(res.L[0])
                                    }

                                }
                            })
                        }
                        else {
                            tip.on("添加失败")
                        }


                    }
                })
            }
            else if(vm.New.Name == ""){
                tip.on('您还没有输入商品名称！！！')
            }

        },
        //取消添加
        addOver: function () {
            vm.New = {
                Name: '',//必填
                PinYin: '',//可选 商品拼音
                Number: '',//商品号，可选
                Standard: '',//规格 可选
                Unit: '',//单位 可选
                BarCode: '',//条码 可选
                Model: '',//型号
                Price1: '',//标价
                Price0:'',//成本
                Max:"",//库存最大值
                Min:"",//库存最小值
                Virtual:0,//是否虚拟商品
                Memo:'',//商品备注
                Amount:''//初始库存
            }
            vm.editClose()
        },


        /*控制商品列表渲染*/
        list: [],

        GoodsId: 0,//商品ID，用户查看商品详情，商品详情与列表的切换也是根据这个值，
        P: 0,
        T: 0,
        N: 48,
        //获取商品列表
        getGoods: function () {
            ws.call({
                i: "Goods/get",
                data: {
                    GoodsIDs: [],
                    P: vm.P + 1,
                    N: vm.N,
                    //Disable:"0,1"
                },
                success: function (res) {
                    if (!res.err) {
                        vm.P = res.P
                        vm.T = res.T
                        if (res.L.length > 0) {
                            for (var i = 0; i < res.L.length; i++) {
                                vm.list.push(res.L[i])
                                console.log(res.L[i].Disable+res.L[i].Name)
                            }
                        }
                        else {
                            vm.P = res.P - 1
                            tip.on('没有更多数据了……', 1, 3000)
                        }

                    }
                    else {
                        console.log(res)
                    }

                }
            })
        },



        //编辑商品
        befor: {},
        editing: -2,
        edit: function (i) {
            //先滚回上一个在编辑的商品
            vm.editClose()


            //然后进行正常的编辑动作

            vm.editing = i
//            vm.befor=vm.list[i]
            var poto = vm.list[i]
            var x;
            for (x in poto) {
                if (x.charAt(0) != '$') {
                    vm.befor[x] = poto[x]
                }
            }

        },
        editClose: function () {
            //撤销修改
            if (vm.editing != -2) {
                var poto = vm.befor
                var x;
                for (x in poto) {
                    if (x.charAt(0) != '$') {
                        if (vm.editing != -7) {
                            try {
                                //在列表修改
                                vm.list[vm.editing][x] = poto[x]
                            }
                            catch (err) {

                            }
                        }
                    }
                }
            }

//            重置当前编辑索引
            vm.editing = -2
        },

        // 重写详情内的编辑
        infoEdit: function (goodsid) {

            vm.editing = -7;
            var poto = vm.info
            var x;
            for (x in poto) {
                if (x.charAt(0) != '$') {
                    vm.befor[x] = poto[x]
                }
            }
        },

        // 保存要也重写
        save: function (i) {


            var after = vm.list[i]

            if (after.Name != '') {
                var befor = vm.befor
                var r2s = {}
                //先判断哪些属性变化了
                var x;
                var priceREG = /^Price+\d*/g
                for (x in after) {
                    if (x.match(priceREG)) {
                        if (after[x] != befor[x]) {
                            //这个是变更的价格
                            vm.editPrice({
                                GoodsID: after.GoodsID,
                                Type: x.replace("Price", ""),
                                Price: after[x]
                            })
                        }
                    }
                    else {
                        if (x.charAt(0) != '$') {
                            if (after[x] != befor[x]) {
                                //记录变化的属性
                                r2s[x] = after[x]
                            }
                        }
                    }


                }

                //发起保存
                function isEmptyObject(obj) {
                    for (var n in obj) {
                        return false
                    }
                    return true;
                }

                if (!isEmptyObject(r2s)) {
                    ws.call({
                        i: "Goods/save",
                        data: {
                            GoodsID: after.GoodsID,
                            Param: r2s
                        },
                        success: function (res) {
                            if (res.err == undefined) {
                                tip.on('保存成功', 1, 3000)
                                vm.befor = after
                                vm.editClose()
                            }
                        }
                    })
                } else {
                    vm.befor = after
                    vm.editClose()
                }

            }
            else {
                tip.on('商品名被吃掉啦！')
                vm.list[i].Name = vm.befor.Name
            }
        },

        //用于保存之前验证库存上下线的正确性
        checkStoreTotal: function (min,max) {
            //验证格式
            function type(num){
                if(num==""||num>=0){
                    return true
                }else{
                    tip.on('库存上下限必须为正数！')
                    return false
                }
            }

            //验证大小
            function minLTmax(min,max){
                if((max-min>0)||max==""||min==""){
                    return true
                }else{
                    tip.on('库存上限必须大于库存下限')
                    return false
                }
            }


            //验证
            if(type(min)&&type(max)&&minLTmax(min,max)){
                return true
            }else{
                return false
            }


        },

        infoSave: function () {
            var after = vm.info



            if (after.Name != ''&&vm.checkStoreTotal(vm.info.Min,vm.info.Max)) {
                var befor = vm.befor
                var r2s = {}
                //先判断哪些属性变化了
                var x;
                for (x in after) {
                    if (x != "Price" && x.charAt(0) != '$') {
                        if (after[x] != befor[x]) {
                            //记录变化的属性
                            r2s[x] = after[x]
                        }
                    }
                }
                //判断是否是在详情里面做修改的，

                for (var j = 0; j < 3; j++) {
                    if (vm.info['price' + j] != '' && vm.info['price' + j] >= 0) {
                        vm.editPrice({
                            GoodsID: after.GoodsID,
                            Type: j,
                            Price: vm.info['price' + j]
                        })
                    }
                }

                function reload() {
                    vm.infoReady(after.GoodsID)
                }

                setTimeout(function () {
                    //发起保存
                    ws.call({
                        i: "Goods/save",
                        data: {
                            GoodsID: after.GoodsID,
                            Param: r2s
                        },
                        success: function (res) {
                            if (res.err == undefined) {
                                tip.on('保存成功', 1, 3000)
                                reload()
                                vm.updateGoods(after)
                            }
                        }
                    })
                }, 1)


                vm.editClose()
            }
            else if(after.Name == ''){
                tip.on('商品名被吃掉啦！')
                vm.info.Name = vm.befor.Name
            }
        },

        //更新列表中的某一条数据
        updateGoods: function (goods) {
            for (var k = 0; k < vm.list.length; k++) {
                if (vm.list[k].GoodsID == goods.GoodsID) {
                    //找到更新的数据
                    var target = vm.list[k], x;
                    for (x in goods) {
                        if (x.charAt(0) != '$') {
                            target[x] = goods[x]
                        }
                    }

                    break
                }
            }
        },

        //商品启用禁用相关
        listType:0,
        toListType:function(i){
            vm.listType=i?Number(!vm.listType):i=="auto"
        },
        changeState:function(i,GoodsID,index){
            ws.call({
                i: "Goods/save",
                data: {
                    GoodsID: GoodsID,
                    Param: {
                        Disable: i
                    }
                },
                success: function (res) {
                    if (!res.err) {
                        vm.list[index].Disable=i
                    }
                    else{
                        tip.on(res.err)
                    }
                }

            })
        },

        /*详情所用*/
        info: {
            price0: '',
            price1: '',
            price2: ''

        },
        $infoKey:{
            "esc":function(){
                vm.infoClose()
            }
        },
        infoClose:function(){
            removeK(vm.$infoKey)
            layout.subClose()
            //if(vm.fromDD == 1){
            //    window.location.href='#!/OrderInfo/'+vm.lastOrderID;
            //    return;
            //}
            //if(vm.fromDD == 2){
            //    setTimeout(function(){
            //        bill.ready("goodsCar")
            //        require(['text!../../package/bill/bill.html'], function (html) {
            //            layout.subUrl = html
            //            layout.subOpen()
            //            avalon.scan();
            //        });
            //    },200);
            //    /***********************************************************************/
            //    /*怎么回到上一个页面*/
            //    return;
            //}
            //if(vm.lastUrl==""){
            //    window.location.href='#!/goods/0'
            //}else{
            //    window.location.href = vm.lastUrl;
            //}

        },
        editInfoType: function (i) {
          vm.info.Virtual=i
        },
        lastUrl:'',
        fromDD:0,
        lastOrderID:'',
        infoReady: function (id) {
             var str = "" + id;
             if(str.indexOf('&') >= 0){
                 vm.fromDD = 1;
                 str = str.split('&');
                 id = parseInt(str[0]);
                 vm.lastOrderID = parseInt(str[1]);
                 vm.getInfo(parseInt(str[1]));
                 vm.computeDDIndex();
             }if(str.indexOf(',') >= 0){
                vm.fromDD = 2;
                str = str.split(',');
                id = parseInt(str[0]);
                vm.getInfo(id);
                vm.computeGCIndex();
            } else {
                 vm.fromDD = 0;
                 id = parseInt(str);
             }

            var tempUrl = window.location.href;
            if(tempUrl.indexOf("goodsInfo")<0){
                vm.lastUrl = window.location.href;
            }

            if (layout.url=="") {
                //require(['../../package/goods/goods'], function () {
                goods.ready();
                //})
            }

            layout.subUrl = info
            layout.rightTitle="商品详情"
            layout.subOpen()
            //modal.url = info
            vm.infoReset()
            //modal.getIn()
            //原N：100
            ws.call({
                i: "Goods/get",
                data: {
                    GoodsIDs: [id],
                    P: 1,
                    N: vm.N

                },
                success: function (res) {
                    if (res.L.length) {

                        vm.buildInfo(res.L[0])
                        vm.buildPrices(res.L[0])
                        vm.buildStore()
                        quickStart.goOut()
                        vm.updateGoods(res.L[0])
                    }
                }
            })
            vm.getClass()
            bindK(vm.$infoKey)
        },

        infoReset: function () {

            vm.info = {
                price0: '',
                price1: '',
                price2: ''
            };

            vm.infoPrices = [
                {
                    Name: "加权成本",
                    Type: 0,
                    Price: "未设置"
                },
                {
                    Name: "标准售价",
                    Type: 1,
                    Price: "未设置"
                },
                {
                    Name: "会员售价",
                    Type: 2,
                    Price: "未设置"
                }
            ]

            vm.editing=-2
            vm.changeing=-1
        },
        toInfo: function (i, edit) {



            vm.GoodsId = i;
            if(vm.curListIndex == 0){
                vm.computeIndex();
            }
            var tempUrl = window.location.href;
            if(tempUrl.indexOf("goodsInfo")<0){
                vm.lastUrl = window.location.href;
            }


            console.log("******************"+vm.lastUrl)


            try{
                pb.startT()
            }catch(err){}
            vm.infoReady(i)
            try{
                pb.endT()
            }catch(err){}

            vm.showClass = false

            if (edit === 1) {
                //进入编辑状态
                setTimeout(function(){
                    vm.infoEdit(vm.info.GoodsID)
                },300)

            }
        },
        toList: function () {
            window.location.href = "#!/goods"
            vm.showClass = false
        },
        buildInfo: function (obj) {
            var poto = obj
            var x;
            var a = {};
            for (x in poto) {
                if (x.charAt(0) != '$') {
                    a[x] = poto[x]
                }
            }
            vm.info = a


        },

        //构建价格
        infoPrices: [
            {
                Name: "加权成本",
                Type: 0,
                Price: "未设置"
            },
            {
                Name: "标准售价",
                Type: 1,
                Price: "未设置"
            },
            {
                Name: "会员售价",
                Type: 2,
                Price: "未设置"
            }
        ],
        buildPrices: function (obj) {
            var poto = obj.Price || null;
            if (poto != null) {

                var readyPoto = {}

                //构建价格，进价为 price0，标价为price1，会员价为price2
                for (var i = 0; i < poto.length; i++) {
                    readyPoto = poto[i];
                    if (readyPoto.Type == 0) {
//                        readyPoto.Name="加权成本"
                        vm.infoPrices[0].Price = readyPoto.Price
                    }
                    else if (readyPoto.Type == 1) {
//                        readyPoto.Name="标准售价"
                        vm.infoPrices[1].Price = readyPoto.Price
                    }
                    else if (readyPoto.Type == 2) {
//                        readyPoto.Name="会员售价"
                        vm.infoPrices[2].Price = readyPoto.Price
                    }


                }
            }


        },

        //编辑价格
        beforP: {},
        changeing: -1,
        changePrice: function (index) {
            //如果是成本则不允许修改
            if (vm.infoPrices[index].Type == 0 && vm.infoPrices[index].Price>0) {
                tip.on("成本价不能在这里编辑", 1, 3000)
            }
            else {
                vm.closeChange()

                vm.changeing = index
                var poto = vm.infoPrices[index]
                var x;
                for (x in poto) {
                    if (x.charAt(0) != '$') {
                        vm.beforP[x] = poto[x]
                    }
                }
            }
        },
        closeChange: function () {


            var poto = vm.beforP
            var x;
            for (x in vm.infoPrices[vm.changeing]) {
                if (x.charAt(0) != '$') {

                    //在列表修改
                    vm.infoPrices[vm.changeing][x] = poto[x]

                }
            }


            vm.changeing = -1
        },
        priceSave: function (index) {
            var obj = {
                GoodsID: vm.info.GoodsID,
                Type: vm.infoPrices[index].Type,
                Price: vm.infoPrices[index].Price
            }

            if(obj.Price>=0||obj.Price==""){
                vm.editPrice(obj)
            }
            else{
                tip.on("价格不能小于0")
            }


        },
//价格修改
        editPrice: function (obj) {
            var GoodsID=obj.GoodsID
            ws.call({
                i: 'Goods/price',
                data: obj,
                success: function (res) {
                    if (!res.err) {
                        tip.on("价格保存成功", 1, 3000)
                        vm.beforP.Price = obj.Price

                        //重新获取并更新商品
                        ws.call({
                            i: "Goods/get",
                            data: {
                                GoodsIDs:GoodsID ,
                                P: 1,
                                N: vm.N
                            },
                            success: function (res) {
                                if(!res.err){
                                    var goods=res.L[0]
                                    vm.updateGoods(goods)
                                }
                            }
                        })
                    }
                    else {
                        tip.on("价格保存失败", 0, 3000)
                        console.log(res)
                    }
                    vm.closeChange()
                }
            })
        },

        //库存
        infoStore: [],
        thisStoreTotal: 0,
        buildStore: function () {
            var nowStore = quickStart.nowStore;

            if(vm.info.Store == undefined){
                return;
            }

            vm.infoStore = vm.info.Store
            var finded = false
            //循环查找
            for (var i = 0; i < vm.infoStore.length; i++) {

                if (vm.infoStore[i].StoreID == nowStore.StoreID) {
                    vm.thisStoreTotal = vm.infoStore[i].Amount
                    finded = true
                    break
                }
            }
            if (!finded) {
                //这是最后一个而且还没有找到
                vm.thisStoreTotal = 0
            }

        },


        /*开单快捷*/
        toBill: function (type) {
            quickStart.start(type)
            bill.getGoodsCarList([vm.info.GoodsID])
        },
        //列表上面的入库
        toBillPur: function (goods) {
            quickStart.start("pur")
            bill.getGoodsCarList([vm.info.GoodsID])
        },


        /*商品类别操作*/
        className: "选择类型",
        classID: '',
        classList: [],
        showClass: false,
        letShowClass: function () {
            if (vm.showClass) {
                vm.showClass = false
            }
            else {
                vm.showClass = true
            }
            vm.classEditing = false
        },
        getClass: function () {
            ws.call({
                i: "Goods/getClass",
                data: {},
                success: function (res) {
                    if (res && !res.err) {
                        vm.classList = res
                    } else {
                        console.log(res)
                    }
                }
            })

        },

        //新加分类
        newClassName: '',
        addClass: function () {
            ws.call({
                i: "Goods/addClass",
                data: {
                    ClassName: vm.newClassName,
                    ParentID: 0
                },
                success: function (res) {
                    if (!res.err) {
                        vm.classList.unshift(res)
                        vm.newClassName = ''

                    }
                    else {
                        console.log(res)
                        tip.on('分类添加失败')
                    }
                }
            })
        },

        //根据分类获取商品
        CP: 0,
        CT: 0,
        byClass: function (id, index) {
            var ClassID
            if (id > 0) {
                vm.classID = id;
                vm.className = vm.classList[index].ClassName
                vm.CP = 0;
                vm.CT = 0
                ClassID = id

                vm.list = []
            } else {
                ClassID = vm.classID
            }

            ws.call({
                i: 'Goods/class',
                data: {
                    GoodsID:[],
                    ClassID: ClassID,
                    Op: "get",
                    P: vm.CP + 1,
                    N: vm.N
                },
                success: function (res) {
                    if (!res.err) {
                        vm.CP = res.P
                        vm.CT = res.T
                        if (res.L != null) {
                            for (var i = 0; i < res.L.length; i++) {
                                vm.list.push(res.L[i])
                            }
                        }
                        else {
                            vm.P = res.P - 1
                        }
                        vm.showClass = false
                    }
                    else {
                        console.log(res)
                    }
                }
            })

        },

        resetClass: function () {
            vm.firstTime = true
            vm.P = 0
            vm.T = 0
            vm.ready()
        },

        //分类的编辑和删除
        classEditing: false,
        toggleEdit: function () {
            if (vm.classEditing) {
                vm.classEditing = false
            }
            else {
                vm.classEditing = true
            }
        },
        CEindex: -1,
        editingName: "",
        toEdit: function (index) {
            vm.CEindex = index
            vm.editingName = vm.classList[index].ClassName
        },
        closeCE: function () {
            vm.CEindex = -1
            vm.editingName = ""
        },
        editingID: "",
        saveClass: function (index) {
            if (vm.classList[index].ClassID > 0) {
                ws.call({
                    i: "Goods/saveClass",
                    data: {
                        ClassID: vm.classList[index].ClassID,
                        Param: {
                            ClassName: vm.editingName
                        }
                    },
                    success: function (res) {
                        if (!res.err) {
                            tip.off("修改出错，请重新尝试……")
                            tip.on("修改成功", 1, 300)
                            vm.getClass()
                            vm.CEindex = -1
                        }
                        else {
                            console.log(res)
                        }
                    }
                })
            } else {
                tip.on("修改出错，请重新尝试……")
                vm.classList = []
                vm.getClass()
                vm.CEindex = -1
            }

        },
        delClass: function (index) {
            ws.call({
                i: "Goods/delClass",
                data: {
                    ClassID: vm.classList[index].ClassID
                },
                success: function (res) {
                    if (!res.err) {
                        if (res) {
                            tip.on("删除成功", 1, 3000)
                            vm.getClass()
                        }
                        else {
                            tip.on("删除失败")
                        }
                    }
                    else {
                        console.log(res)
                    }
                }
            })
        },

        //更改商品的分类
        changeClass: function (CID, CN) {
            ws.call({
                i: "Goods/class",
                data: {
                    ClassID: CID,
                    GoodsIDs: [vm.info.GoodsID],
                    Op: "add"
                },
                success: function (res) {
                    if (!res.err) {
                        tip.on("该商品的分类已调整为" + CN, 1, 3000)
                        vm.infoReady(vm.info.GoodsID)

                    } else {
                        console.log(res)
                    }
                }
            })
        },

        curListIndex:0,
        computeIndex:function(){
            for(var i = 0;i < vm.list.length;i ++){
                if(vm.list[i].GoodsID == vm.GoodsId){
                    vm.curListIndex = i;
                    break;
                }
            }
        },
        lookLastGood:function(){
            var i = 0;
            if(vm.curListIndex > 0){
                vm.curListIndex --;
                i = vm.list[vm.curListIndex].GoodsID;

                vm.toInfo(i);
            }
        },
        lookNextGood:function(){
            var i = 0;
            if(vm.curListIndex < vm.list.length - 1){
                vm.curListIndex ++;
                i = vm.list[vm.curListIndex].GoodsID;

                vm.toInfo(i);
            }
        },


        /*以下是从订单查看商品的逻辑*/
        DDIndex:0,
        computeDDIndex:function(){
            for(var i = 0;i < vm.infoGoo.length;i ++){
                if(vm.infoGoo[i].GoodsID == vm.GoodsId){
                    vm.DDIndex = i;
                    break;
                }
            }
        },
        lookLastDD:function(){
            console.log("--------------------------上一个订单的商品")
            var i = 0;
            if(vm.DDIndex > 0){
                vm.DDIndex --;
                vm.GoodsId = vm.infoGoo[vm.DDIndex].GoodsID;

                vm.info = vm.infoGoo[vm.DDIndex];
            }
        },
        lookNextDD:function(){
            console.log("--------------------------下一个订单的商品")
            var i = 0;
            if(vm.DDIndex < vm.infoGoo.length - 1){
                vm.DDIndex ++;
                vm.GoodsId = vm.infoGoo[vm.DDIndex].GoodsID;

                vm.info = vm.infoGoo[vm.DDIndex];
            }
        },
        infoGoo:[],
        getInfo: function (id) {
            vm.infoGoo = [];
            ws.call({
                i: "Order/get",
                data: {
                    OrderIDs: [id],
                    P: 1,
                    N: 20
                },
                success: function (res) {
                    if (!res.err) {
                        console.log("--------------------\\\\\\\\\\\\\\\\\\========");
                        console.log(res);

                        //获取订单内商品列表
                        var l = res.L[0].Goods;
                        for (var x in l) {
                            vm.infoGoo.push(l[x]);
                        }
                    } else {console.log(res.err)}
                }
            });
        },
        /*备选商品的上下一个商品*/
        GCIndex:"",
        computeGCIndex:function(){
            for(var i = 0;i < layout.goodsCarList.length;i ++){
                if(layout.goodsCarList[i].GoodsID == vm.GoodsId){
                    vm.GCIndex = i;
                    break;
                }
            }
        },
        lookLastGCar:function(){
            console.log("------------备选商品--------------上一个订单的商品")
            var i = 0;
            if(vm.GCIndex > 0){
                vm.GCIndex --;
                vm.GoodsId = layout.goodsCarList[vm.GCIndex].GoodsID;

                vm.info = layout.goodsCarList[vm.GCIndex];
            }
        },
        lookNextGCar:function(){
            console.log("-------------备选商品-------------下一个订单的商品")
            var i = 0;
            if(vm.GCIndex < layout.goodsCarList.length - 1){
                vm.GCIndex ++;
                vm.GoodsId = layout.goodsCarList[vm.GCIndex].GoodsID;

                vm.info = layout.goodsCarList[vm.GCIndex];
            }
        },
        //加入备选商品车
        addToCar:function(index){
            /*防止重复*/

            require(['../../package/goods/selected'],function(){
                for(var i = 0;i < selected.goodsCarList.length;i ++){
                    if(selected.goodsCarList[i].GoodsID == vm.list[index].GoodsID){
                        return;
                    }
                }
                selected.newSelected(vm.list[index].GoodsID)
            })

        }
    });
    return goods = vm;
});
/**
 * Created by mooshroom on 2015/8/11.
 */
define('store',
    [
        'avalon',
        'text!../../package/store/store.html',
        'css!../../package/store/store.css'
    ], function (avalon, html, css) {
        var vm=avalon.define({
            $id:"store",
            ready: function () {
                layout.url=html;
                vm.getStores()

                avalon.scan()
            },

            /*添加*/
            newName:'',
            newAdmin:'',
            newPhone:'',
            addStore: function () {
                if(vm.newName!=""){
                    ws.call({
                        i:"Store/add",
                        data:{
                            Name:vm.newName,
                            Admin:vm.newAdmin,
                            Phone:vm.newPhone,
                        },
                        success: function (res) {
                            if(!res.err){
                                tip.on("新店铺添加成功！",1,3000);
                                vm.ready()
                                quickStart.getStores()
                            }
                        }
                    })
                }
            },



            /*列表*/
            list:[],
            //获取库房
            getStores: function () {
                ws.call({
                    i:"Store/get",
                    data:{
                        StoreIDs:[]
                    },
                    success: function (res) {

                            if(!res.err){
                                vm.list=res
                            }
                            else{
                                console.log(res)
                            }


                    }
                })
            },

        })
    })
/**
 * Created by Administrator on 2015/8/4 0004.
 */
define('orderInfo',['avalon','text!../../package/order/info.html','css!../order/order.css'], function (avalon,info) {
    var vm=avalon.define({
        $id:"orderInfo",
        /*订单详情*/
        ready:function(OrderID){

            vm.reset();

            //modal.url=info
            //modal.getIn()
            layout.subUrl=info
            layout.rightTitle="账单详情"
            layout.subOpen()

            avalon.scan()

            vm.thisOrderId = OrderID;
            vm.getInfo(OrderID)

            vm.bindKey()

        },
        thisOrderId:"",
        $key:{
            "esc":function(){
                vm.close()
            }
        },
        binding:false,
        bindKey:function(){

                bindK(vm.$key)

        },
        removeKey:function(){

                removeK(vm.$key)

        },
        reset: function () {
            vm.showLNBtn = false;

            vm.info={};
            vm.infoCus={};
            vm.infoGoo=[];
            vm.Money=null;
            vm.settleShow=true

            //重置结算记录详情
            vm.detailreset()
        },
        info:{},
        infoCus:{},
        infoGoo:[],
        bigTotal:"",
        getInfo: function (id) {
            ws.call({
                i:"Order/get",
                data:{
                    OrderIDs:[id],
                    P:1,
                    N:20
                },
                success:function(res){
                    if(!res.err){

                        try{
                            order.updataOrders(res.L[0])
                        }catch(err){
                            console.log(err)
                        }


                        if(res.L[0].OrderCode!="A"&&res.L[0].OrderCode!="-4"&&res.L[0].OrderCode!="B"){
                            vm.infoCus=res.L[0].Customer;
                            res.L[0].Total=Math.abs(res.L[0].Total)
                            res.L[0].Payed=Math.abs(res.L[0].Payed)
                        }
                        vm.info=res.L[0]
                        vm.bigTotal=trs(Number(vm.info.Total))
                        var l=res.L[0].Goods
                        console.log(l)
                        var x;
                        for(x in l){
                            vm.infoGoo.push(l[x]);
                        }
                        //vm.infoGoo=l






                    }
                    else{
                        console.log(res.err)
                    }
                }
            })


            function trs(num) {
                var word = ["零 ", "壹", "贰", "叁", "	肆", "伍", "陆", "柒", "捌", "玖"]
                var u1 = ["", "拾", "佰", "仟"]
                var u2 = ["圆", "万", "亿"]
                var end = ['角', '分', '整']
                num = num<0?-num:num;
                var bStr = num.toString()

                var dot_i = bStr.indexOf(".")

                var int = "",//整数部分
                    float = "";//小数部分

                var str2 = "";

                if (dot_i == -1 || dot_i == bStr.length - 1) {
                    //无小数位
                    int = bStr;
                    str2 = end[2]
                }
                else {
                    //有小数位
                    int = bStr.slice(0, dot_i)
                    float = bStr.slice(dot_i + 1)

                    var jiao = float[0] | "0"
                    var fen = float[1] | "0"

                    str2 = word[jiao] + end[0] + word[fen] + end[1]

                }

                var str1 = "";
                var r1 = 0
                var r2 = 0
                var up = true
                var o = ""
                var pp = ""
                var p = ""
                var w = ""

                for (var i = int.length - 1; i > -1; i--) {
                    o = int[i]
                    p = u1[r1]

                    w = word[o] + p

                    if (up == true) {
                        pp = u2[r2]
                        up = false
                    }
                    else {
                        pp = ""
                    }
                    str1 = w + pp + str1

                    r1++
                    if (r1 == u1.length) {
                        r1 = 0
                        r2++
                        up = true
                    }

                }
                var str = str1 + str2
                return str.replace(/ /g, "")
            }
        },
        
        
        /********************结算*********************/
        showSettle:function(){
            vm.settleShow=true
        },
        settleShow:true,
        
        hideSettle: function () {
            vm.settleShow=false
            vm.detailreset();   //重置详情
        },
        Money:null,
        settle: function () {

            if(vm.Money<=0){
                tip.on('结算金额必须大于0')
            }
            else if(vm.Money>(Number(vm.info.Total)-Number(vm.info.Payed))){

                tip.on('结算金额超出了所需要结算的额度')
                //if(confirm("您将要支付的金额超过未结算的金额，多余的金额将不会被计算，是否继续？")){
                //    push()
                //}
            }
            else if(isIt.money(vm.Money,"结算金额")){
                //付款
                push()
            }

            function push() {
                var money=0;
                if(vm.info.OrderCode>0){
//                付款
                    money=-vm.Money
                }
                else if(vm.info.OrderCode<0){
                    money=vm.Money
                }

                ws.call({
                    i:"Order/settle",
                    data:{
                        OrderIDs:[vm.info.OrderID],
                        CustomerID:vm.info.CustomerID,//客户编号
                        Money:money//正数表示收款，负数表示付款
                    },
                    success: function (res) {
                        if(!res.err){
                            tip.on("结算成功!!",1)
                            vm.hideSettle();

                            if(/^#!\/order\//g.test(window.location.hash)){
                                // 当前在总账记录页面
                                order.showNewPayed(vm.Money)
                            }
                            else if(/^#!\/customerInfo\//g.test(window.location.hash)){
                                // 当前在客户详情页面
                                customer.uploadMoney(vm.Money)
                            }

                            vm.ready(vm.info.OrderID);

                        }
                        else{
                            console.log(res)
                        }
                    }
                })
            }

        },
        /********************打印*********************/
print: function (OrderID) {
            vm.detailreset();   //重置详情
    //bdhtml=window.document.body.innerHTML; //获取当前页的html代码
    //sprnstr="<!--startprint-->"; //设置打印开始区域
    //eprnstr="<!--endprint-->"; //设置打印结束区域
    //prnhtml=bdhtml.substr(bdhtml.indexOf(sprnstr)+17); //从开始代码向后取html
    //prnhtml=prnhtml.substring(0,prnhtml.indexOf(eprnstr)); //从结束代码向前取html
            ws.call({
                i:"Order/print",
                data:{
                    OrderID:OrderID
                },
                success:function(res){
                    if(!res.err){
                        //var print=document.getElementById("print")
                        //print.innerHTML=res;
                        //window.print();
                        tip.on("正在使用打印客户端打印",1)
                    }
                }

            })



},
        //结算记录
        showDetail:false,
        details:[],
        detailreset:function(){
            vm.showDetail = false;
            vm.details = [];
        },
        openDetail:function(){
            if(vm.showDetail){
                vm.showDetail = false;
            }else{
                vm.showDetail = true;
                ws.call({
                    i:"Order/settleLog",
                    data:{
                        OrderID:vm.info.OrderID
                    },
                    success:function(res){
                        if(!res.err){
                            //console.log(info.OrderID + "这是详情！！！" + res);
                            vm.details = res;
                        }else{
                            tip.on("取得详情失败!")
                        }
                    }
                });
            }
        },

        //关闭的动作
        close: function () {
            vm.removeKey()
            //window.location.href = '#!/order/0'
            layout.subClose()

        }

        
        
    })
    return orderInfo = vm;
})
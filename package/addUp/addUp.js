/**
 * Created by mooshroom on 2015/8/31.
 */
define('addUp',[
    'avalon',
    'text!../../package/addUp/addUp.html',
    'css!../../package/addUp/addUp.css'
],function(avalon,html){
    var vm=avalon.define({
        $id:"addUp",
        ready:function(i){
            layout.url=html;

            vm.tableList[i].start()
            //vm.toTable(0)
            vm.nowTable=i
        },

        //表格列表
        tableList:[
            {
                name:'销售毛利汇总',
                href:"#!/addUp/0",
                start:function(){
                    setTimeout(function(){
                        if(vm.nowSellType==-1){
                            vm.toSellType(0)
                        }
                    },1)

                }
            },
            {
                name:'应收应付汇总',
                href:"#!/addUp/1",
                start:function(){
                    setTimeout(function(){
                        if(vm.outList.length+vm.inList.length==0){
                            //两个都是空的
                            vm.getOut()
                            vm.getIn()
                        }
                    },1)

                }
            },
            {
                name:'采购类报表',
                href:"#!/addUp/2",
                start:function(){

                }
            },
            {
                name:'库存报表',
                href:"#!/addUp/3",
                start:function(){


                }
            },
            {
                name:'销售类报表',
                href:"#!/addUp/4",
                start:function(){


                }
            }
        ],
        //当前活动的表格
        nowTable:0,

        //跳转到某个表格
        toTable:function(index){
        //    var t1=new Date().getTime()
            vm.nowTable = index;
        //    var t2=new Date().getTime()
        //    console.log(t2-t1)
            setTimeout(function(){
                vm.tableList[index].start();
            },1);

            console.log("--------- : " + vm.nowTable);

        },


        /*****************销售毛利统计汇总********************/
        sellType:[
            {
                name:"客户",
                g:'Customer',
                start:function(){
                    setTimeout(function(){
                        vm.getSellList()
                    },1)

                }
            },
            {
                name:"库房",
                g:'Store',
                start:function(){
                    setTimeout(function(){
                        vm.getSellList()
                    },1)

                }
            },
            {
                name:"开单人",
                g:'Operator',
                start:function(){
                    setTimeout(function(){
                        vm.getSellList()
                    },1)

                }
            },
            {
                name:"商品",
                g:'Goods',
                start:function(){
                    setTimeout(function(){
                        vm.getSellList()
                    },1)

                }
            }
        ],
        //跳转指定的类型
        nowSellType:-1,
        toSellType: function (index) {
            vm.nowSellType=index;
            setTimeout(function(){
                vm.sellType[index].start()
            })

        },

        //统计表显示数据
        sellList:[],
        allST:'',
        allSC:'',
        allSP:'',
        allSN:'',

        sellSum: function () {
            var allST= 0,
                allSC= 0,
                allSP= 0,
                allSN= 0;
            for(var i= 0;i<vm.sellList.length;i++){
                allSN+=Number(vm.sellList[i].Ordercount)
                allST+=Number(vm.sellList[i].Total)
                allSC+=Number(vm.sellList[i].Cost)
                allSP+=Number(vm.sellList[i].Profit)

            }
            vm.allST=allST.toFixed(2)
            vm.allSC=allSC.toFixed(2)
            vm.allSP=allSP.toFixed(2)
            vm.allSN=allSN

        },


        //时间选择控制

        startDate:"",
        endDate:"",


        //获取统计表 G：统计分组项：四选一(Customer,Operator,Store,Goods)，
        // 分别代表 客户/供应商，业务员、库房
        getSellList:function(){
            var s,e;
            function call(s,e){
                //发起请求
                vm.sellList=[]
                vm.allST=0
                vm.allSC=0
                vm.allSP=0
                vm.allSN=0
                ws.call({
                    i:"Report/tradeMoney",
                    data:{
                        S:s,
                        E:e,
                        G:vm.sellType[vm.nowSellType].g,
                        OrderCode:['in','-1,2']
                    },
                    success:function(res){
                        if(res){
                            //获取成功
                            //清空原有数组
                            vm.sellList=res
                            vm.sellSum()
                        }
                        else{
                            console.log(res)

                        }
                    }
                })
            }

            if(vm.startDate!=""&&vm.endDate!=""){
                s=(newDateAndTime(vm.startDate).getTime()/1000).toFixed()
                e=(newDateAndTime(vm.endDate).getTime()/1000).toFixed()
                if(s<=e){
                    call(s,e)
                }else{
                    call(e,s)
                }
            }

        },


        /*****************应收应付汇总********************/
            //指令：Report/customer  参数 R ,true:应收款,false应付款

        outList:[],
        totalOut:'',
        getOut: function () {

                //获取应付
                ws.call({
                    i:"Report/customer",
                    data:{
                        R:false
                    },
                    success: function (res) {
                        vm.outList=[]
                        if(res){
                            vm.outList=res
                            var t=0
                            for(var i=0;i<res.length;i++){
                                t=t+Number(res[i].Payable)
                            }
                            vm.totalOut= t.toFixed(2)
                        }
                    }
                })

        },
        inList:[],
        totalIn:'',
        getIn: function () {

                //获取应收
                ws.call({
                    i:"Report/customer",
                    data:{
                        R:true
                    },
                    success: function (res) {
                        vm.inList=[]
                        if(res){
                            vm.inList=res

                            var y=0
                            for(var o=0;o<res.length;o++){
                                y=y+Number(res[o].Receivables)
                            }
                            vm.totalIn= y.toFixed(2)
                        }
                    }
                })


        },
        /*****************采购********************/
        buyType:[
            {
                name:"商品",
                g:'Goods',
                start:function(){

                }
            },
            {
                name:"供应商",
                g:'Store',
                start:function(){

                }
            },
            {
                name:"开单人",
                g:'Operator',
                start:function(){


                }
            },
            {
                name:"订单",
                g:'Order',
                start:function(){


                }
            }
        ],
        //采购跳转指定的类型
        buyFirst0:true,
        buyFirst1:true,
        buyFirst2:true,
        buyFirst3:true,
        nowBuyType:-1,
        toBuyType: function (index) {
            vm.nowBuyType = index;
            vm.buyType[index].start()
        },
        /*****************库存********************/
        stockType:[
            {
                name:"库存",
                g:'Goods',
                start:function(){

                }
            },
            {
                name:"库存收发汇总",
                g:'Store',
                start:function(){

                }
            },
            {
                name:"库存收发明细",
                g:'Operator',
                start:function(){


                }
            }
        ],
        //库存跳转指定的类型
        stockFirst0:true,
        stockFirst1:true,
        stockFirst2:true,
        nowStockType:-1,
        toStockType: function (index) {
            vm.nowStockType = index;
            vm.buyType[index].start()
        },
        /*****************销售********************/
        saleType:[
            {
                name:"单位名称",
                g:'Goods',
                start:function(){

                }
            },
            {
                name:"开单人",
                g:'Store',
                start:function(){

                }
            },
            {
                name:"商品名称",
                g:'Operator',
                start:function(){


                }
            },
            {
                name:"往来单位欠款单",
                g:'Operator',
                start:function(){


                }
            }
        ],
        //销售跳转指定的类型
        saleFirst0:true,
        saleFirst1:true,
        saleFirst2:true,
        saleFirst3:true,
        nowSaleType:-1,
        toSaleType: function (index) {
            vm.nowSaleType = index;
            vm.buyType[index].start()
        },
        yesToSearch:function(){
            if(vm.nowBuyType == 0){
                vm.buyFirst0 = false;
            }
            if(vm.nowBuyType == 1){
                vm.buyFirst1 = false;
            }
            if(vm.nowBuyType == 2){
                vm.buyFirst2 = false;
            }
            if(vm.nowBuyType == 3){
                vm.buyFirst3 = false;
            }

            if(vm.nowStockType == 0){
                vm.stockFirst0 = false;
            }
            if(vm.nowStockType == 1){
                vm.stockFirst1 = false;
            }
            if(vm.nowStockType == 2){
                vm.stockFirst2 = false;
            }

            if(vm.nowSaleType == 0){
                vm.saleFirst0 = false;
            }
            if(vm.nowSaleType == 1){
                vm.saleFirst1 = false;
            }
            if(vm.nowSaleType == 2){
                vm.saleFirst2 = false;
            }
            if(vm.nowSaleType == 3){
                vm.saleFirst3 = false;
            }

        }




    })
    return addUp=vm
})